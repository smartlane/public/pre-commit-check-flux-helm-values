import os
import subprocess
import sys
import tempfile
import yaml
import argparse
from typing import Sequence

errors: list = []


def _add_repo(args):
    helm_repo_add_cmd = f"helm repo add {args.repo_name} {args.repo_url} " \
                        f"--username {os.environ['CHARTMUSEUM_USER']} " \
                        f"--password {os.environ['CHARTMUSEUM_PW']}"

    print(helm_repo_add_cmd)
    with tempfile.TemporaryDirectory() as tmpDir:
        res = subprocess.run(
            f"{helm_repo_add_cmd}",
            shell=True,
            cwd=tmpDir,
            text=True,
            stdout=subprocess.PIPE,
            stderr=subprocess.STDOUT,
        )
        if res.returncode != 0:
            _collect_errors(
                {"source": "helm repo add", "message": f"\n{res.stdout}"}
            )


def main(argv: Sequence[str] | None = None) -> int:
    parser = argparse.ArgumentParser()
    parser.add_argument(
        '--repo-name',
        default='smartlane'
    )
    parser.add_argument(
        '--repo-url',
        default='https://chartmuseum.ci.smartlane.io',
        required='--repo-name' in sys.argv
    )
    parser.add_argument('filenames', nargs='*', help='Filenames to check.')
    args = parser.parse_args(argv)

    _add_repo(args)
    for filename in args.filenames:
        try:
            _validate_file(filename, args)
        except Exception as ex:
            _collect_errors({"source": filename, "message": f"{type(ex).__name__} {ex.args}"})
    if len(errors) > 0:
        _print_errors()
        return 1
    else:
        return 0


def _validate_file(fileToValidate, args):
    with open(fileToValidate) as f:
        for definition in yaml.load_all(f, Loader=yaml.SafeLoader):
            if (
                    not definition
                    or "kind" not in definition
                    or definition["kind"] != "HelmRelease"
            ):
                continue

            chartSpec = definition["spec"]["chart"]["spec"]

            if chartSpec["sourceRef"]["kind"] != "HelmRepository":
                continue

            chartName = chartSpec["chart"]
            chartVersion = chartSpec["version"]

            with tempfile.TemporaryDirectory() as tmpDir:
                with open(os.path.join(tmpDir, "values.yaml"), "w") as valuesFile:
                    if "spec" in definition and "values" in definition["spec"]:
                        yaml.dump(definition["spec"]["values"], valuesFile)

                        res = subprocess.run(
                            f"helm pull {args.repo_name}/{chartName} --version {chartVersion}",
                            shell=True,
                            cwd=tmpDir,
                            text=True,
                            stdout=subprocess.PIPE,
                            stderr=subprocess.STDOUT,
                        )

                        if res.returncode != 0:
                            _collect_errors(
                                {"source": "helm pull", "message": f"\n{res.stdout}"}
                            )
                            continue

                        res = subprocess.run(
                            "helm lint -f values.yaml *.tgz",
                            shell=True,
                            cwd=tmpDir,
                            text=True,
                            stdout=subprocess.PIPE,
                            stderr=subprocess.STDOUT,
                        )
                        print(res.stdout)
                        if res.returncode != 0:
                            _collect_errors(
                                {"source": "helm lint", "message": f"\n{res.stdout}"}
                            )


def _collect_errors(error):
    errors.append(error)


def _print_errors():
    for i in errors:
        print(f"[ERROR] {i['source']}: {i['message']}")


if __name__ == "__main__":
    raise SystemExit(main())
